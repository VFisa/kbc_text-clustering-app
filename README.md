# README #

This is a public package of the work by Jordan Brace that I contributed to:
[R text Clustering](https://bitbucket.org/jordanbrace/kbc_rtextclustering/)

### What is this repository for? ###

* Define text clusters based on Levenshtein nearest neighbor value